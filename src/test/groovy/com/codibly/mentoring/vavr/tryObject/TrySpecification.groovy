package com.codibly.mentoring.vavr.tryObject

import com.codibly.mentoring.modules.utils.error.MyError
import com.codibly.mentoring.modules.utils.error.NotFound
import groovy.util.logging.Slf4j
import io.vavr.control.Either
import io.vavr.control.Try
import spock.lang.Specification

@Slf4j
class TrySpecification extends Specification {


    def "Should friendly use method which throws checked exception"() {
        when:
        Either<MyError, byte[]> either = Try.of(() -> openFile())
                .onFailure(throwable -> log.error("Exception occured {}", throwable))
                .map(e -> e.getBytes())
                .toEither(new NotFound("File not found in our filesystem"))

        then:
        either.isLeft()
        either.getLeft().getMessage() == "File not found in our filesystem"
    }

    //TODO: co się jak zostanie rzucony uncheked exception

    private FileInputStream openFile() throws FileNotFoundException {
        def file = new File("notExistingFile.txt")
        return new FileInputStream(file)
    }
}
