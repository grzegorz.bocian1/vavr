package com.codibly.mentoring.vavr.collections

import io.vavr.collection.List
import spock.lang.Specification

class ListSpecification extends Specification {

    def "Should create new list instead expand old one"() {
        given:
        var basicList = List.of("Ala", "ma", "Kota")

        when:
        var expendedList = basicList.append("a kot ma Alę")

        then:
        basicList.size() == 3
        expendedList.size() == 4
    }

    def "Should modify my list"() {
        // 50% moich metod to praca na liście którą zamieniam na stream - syntatic sugar
        when:
        var myList = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .filter(e -> e % 2 == 0)
                .map(e -> e * 10)

        then:
        myList == List.of(20, 40, 60, 80, 100)
    }

    def "Should return list with values"() {
        when:
        //TODO: do a filter in stream
        var myList = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .filter(e -> e > 10)
                .orElse(List.of(11))
        then:
        myList == List.of(11)
    }
}
