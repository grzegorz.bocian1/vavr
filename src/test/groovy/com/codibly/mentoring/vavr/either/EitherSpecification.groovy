package com.codibly.mentoring.vavr.either

import io.vavr.control.Either
import spock.lang.Specification

class EitherSpecification extends Specification {

    def "Should return NoSuchElementException for left value"() {
        given:
        Either<String, String> either = Either.right("Ala ma kota")

        when:
        either.getLeft()

        then:
        var exception = thrown(NoSuchElementException.class)
        exception instanceof NoSuchElementException
        exception.message == "getLeft() on Right"
    }
}
