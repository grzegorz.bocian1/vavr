package com.codibly.mentoring.modules.users;

import com.codibly.mentoring.modules.utils.error.MyError;
import com.codibly.mentoring.modules.users.domain.UserDomain;
import io.vavr.control.Either;

import java.util.List;

public interface UsersFacade {

    List<UserDomain> getUsers();

    Either<MyError, List<UserDomain>> getUsersEither();
}
