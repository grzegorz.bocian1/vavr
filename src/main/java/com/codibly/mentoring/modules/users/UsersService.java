package com.codibly.mentoring.modules.users;

import com.codibly.mentoring.modules.users.exceptions.UserNotFoundException;
import com.codibly.mentoring.modules.utils.error.MyError;
import com.codibly.mentoring.modules.utils.error.NotFound;
import com.codibly.mentoring.modules.users.domain.UserDomain;
import io.vavr.control.Either;

import java.util.List;

class UsersService implements UsersFacade {

    @Override
    public List<UserDomain> getUsers() {
        throw new UserNotFoundException("No users initialized in database");
    }

    @Override
    public Either<MyError, List<UserDomain>> getUsersEither() {
        return Either.left(new NotFound("No users initialized in database"));
    }
}
