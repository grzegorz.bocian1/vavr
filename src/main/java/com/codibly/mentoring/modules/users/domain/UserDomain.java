package com.codibly.mentoring.modules.users.domain;

import lombok.Value;

@Value(staticConstructor = "of")
public class UserDomain {
    String name;
    String surname;
}
