package com.codibly.mentoring.modules.users;

public class UsersFacadeConfiguration {

    public UsersFacade createUserFacade(){
        return new UsersService();
    }
}
