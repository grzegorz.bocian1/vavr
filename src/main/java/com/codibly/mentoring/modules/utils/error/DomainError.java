package com.codibly.mentoring.modules.utils.error;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@RequiredArgsConstructor
public class DomainError implements MyError {

    private String message;
    private final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
}
