package com.codibly.mentoring.modules.utils.error;


import org.springframework.http.HttpStatus;

public interface MyError {
    String getMessage();
    HttpStatus getHttpStatus();
}
