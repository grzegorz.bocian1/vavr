package com.codibly.mentoring.infrastructure.api.external.users;

import com.codibly.mentoring.modules.users.domain.UserDomain;
import com.codibly.mentoring.infrastructure.api.external.users.dto.ExternalUser;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ExternalUserMapper {
    List<ExternalUser> toExternalUsers(List<UserDomain> userDomains);
}
