package com.codibly.mentoring.infrastructure.api.internal.users.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InternalUser {
    String name;
    String surname;
}
