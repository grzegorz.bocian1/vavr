package com.codibly.mentoring.infrastructure.api.external.users;

import com.codibly.mentoring.modules.users.UsersFacade;
import com.codibly.mentoring.infrastructure.api.external.ExternalResponse;
import com.codibly.mentoring.infrastructure.api.external.ExternalResponseFactory;
import com.codibly.mentoring.infrastructure.api.external.users.dto.ExternalUser;
import com.codibly.mentoring.modules.utils.error.MyError;
import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Service
@RestController
@AllArgsConstructor
public class ExternalUserController {

    private UsersFacade userFacade;
    private ExternalUserMapper externalUserMapper;

    @GetMapping("/external/users")
    public ResponseEntity<ExternalResponse<List<ExternalUser>>> getUsers(){
        Either<MyError, List<ExternalUser>> users = userFacade.getUsersEither()
                .map(userDomains -> externalUserMapper.toExternalUsers(userDomains));
        return ExternalResponseFactory.of(users);
    }

}
