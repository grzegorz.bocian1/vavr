package com.codibly.mentoring.infrastructure.api.internal.users;

import com.codibly.mentoring.infrastructure.api.internal.InternalResponse;
import com.codibly.mentoring.infrastructure.api.internal.InternalResponseFactory;
import com.codibly.mentoring.infrastructure.api.internal.users.dto.InternalUser;
import com.codibly.mentoring.modules.users.UsersFacade;
import com.codibly.mentoring.modules.utils.error.MyError;
import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Service
@RestController
@AllArgsConstructor
public class InternalUserController {

    private UsersFacade userFacade;
    private InternalUserMapper internalUserMapper;

    @GetMapping("/internal/users")
    public ResponseEntity<InternalResponse<List<InternalUser>>> getUsers(){
        Either<MyError, List<InternalUser>> users = userFacade.getUsersEither()
                .map(userDomains -> internalUserMapper.toInternalUsers(userDomains));
        return InternalResponseFactory.of(users);
    }

}
