package com.codibly.mentoring.infrastructure.api.internal;

import com.codibly.mentoring.modules.utils.error.MyError;
import io.vavr.control.Either;
import lombok.experimental.UtilityClass;
import org.springframework.http.ResponseEntity;

@UtilityClass
public class InternalResponseFactory {

    public static <T> ResponseEntity<InternalResponse<T>> of(Either<MyError, T> either) {
        if (either.isRight()) {
            return ResponseEntity.ok(new InternalResponse<>(either.get()));
        } else {
            MyError error = either.getLeft();
            return ResponseEntity.status(error.getHttpStatus())
                    .body(new InternalResponse<>(error.getHttpStatus(), error.getMessage()));
        }
    }
}
