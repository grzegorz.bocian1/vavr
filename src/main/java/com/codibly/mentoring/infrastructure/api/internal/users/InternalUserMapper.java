package com.codibly.mentoring.infrastructure.api.internal.users;

import com.codibly.mentoring.infrastructure.api.internal.users.dto.InternalUser;
import com.codibly.mentoring.modules.users.domain.UserDomain;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface InternalUserMapper {
    List<InternalUser> toInternalUsers(List<UserDomain> userDomains);
}
