package com.codibly.mentoring.infrastructure.api.external;

import com.codibly.mentoring.modules.utils.error.MyError;
import io.vavr.control.Either;
import lombok.experimental.UtilityClass;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

@UtilityClass
public class ExternalResponseFactory {

    public static <T> ResponseEntity<ExternalResponse<T>> of(Either<MyError, T> either) {
        if (either.isRight()) {
            return ResponseEntity.ok(new ExternalResponse<>(either.get(), LocalDateTime.now()));
        } else {
            MyError error = either.getLeft();
            return ResponseEntity.status(error.getHttpStatus())
                    .body(new ExternalResponse<>(error.getHttpStatus().value(), error.getMessage(), LocalDateTime.now()));
        }
    }
}
