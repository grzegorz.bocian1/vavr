package com.codibly.mentoring.infrastructure.api.external.users.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExternalUser {
    String name;
    String surname;
}
