package com.codibly.mentoring.infrastructure.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class ExternalResponse<T> {
    T data;
    @JsonProperty("status_code")
    int statusCode;
    @JsonProperty("status_message")
    String message;
    LocalDateTime timestamp;

    public ExternalResponse(T data, LocalDateTime timestamp) {
        this.data = data;
        this.timestamp = timestamp;
    }

    public ExternalResponse(int statusCode, String message, LocalDateTime timestamp) {
        this.statusCode = statusCode;
        this.message = message;
        this.timestamp = timestamp;
    }
}
