package com.codibly.mentoring.infrastructure.api.internal;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class InternalResponse<T> {

    private T content;
    private String message;
    private HttpStatus httpStatus;

    public InternalResponse(T content) {
        this.content = content;
    }

    public InternalResponse(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

}
