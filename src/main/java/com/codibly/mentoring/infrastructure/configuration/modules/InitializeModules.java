package com.codibly.mentoring.infrastructure.configuration.modules;

import com.codibly.mentoring.modules.users.UsersFacade;
import com.codibly.mentoring.modules.users.UsersFacadeConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitializeModules {

    @Bean
    public UsersFacade usersFacade(){
        return new UsersFacadeConfiguration().createUserFacade();
    }
}
