package com.codibly.mentoring;

import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@SpringBootApplication
public class MentoringApplication {

    public static void main(String[] args) {
        SpringApplication.run(MentoringApplication.class, args);

//        Optional.of("Ala ma kota")
//                .map(s -> throwException());
//
//        Either.<Exception, String>right("Ala ma kota")
//                .flatMap(s -> throwError());
    }


    private static String throwException() throws IOException {
        throw new IOException();
    }

    private static Either<Exception, String> throwError() {
        try {
            throw new IOException();
        } catch (IOException e) {
            log.error("{ }", e);
            return Either.left(e);
        }
    }
}
